#!/usr/bin/env python3

"""
playpen
Copyright (C) 2021 LoveIsGrief

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

import argparse
import json
import os
import subprocess
from pathlib import Path

parser = argparse.ArgumentParser(
    description="Event handler for https://gitlab.com/NamingThingsIsHard/music-tools/picard-watcher/"
                " to write the error to a file beside the source and then notify nextcloud"
)

parser.add_argument("--config", type=argparse.FileType())
parser.add_argument("event_type", help="Current possibilities handled: moved,error")
parser.add_argument("event_args", nargs="*", help="args passed to the event")

args = parser.parse_args()

if args.config:
    config = json.load(args.config)
else:
    config = json.loads(Path("~/.config/picard_watcher/event_handler.json").expanduser().read_text())

expected_keys = [
    ("handled_errors", list),
    ("nextcloud_absolute_folder", str),
    ("nextcloud_container", str),
    ("nextcloud_container_folder", str),
]

for expected_key, val_type in expected_keys:
    assert expected_key in config
    value = config[expected_key]
    if not isinstance(value, val_type):
        print(f"config: {expected_key} must have value of type {val_type}")
        exit(1)
    if not value:
        print(f"config: {expected_key} must have a value")
        exit(1)

nextcloud_container: str = config["nextcloud_container"]
nextcloud_container_folder: Path = Path(config["nextcloud_container_folder"])
nextcloud_absolute_folder: Path = Path(config["nextcloud_absolute_folder"])
if not nextcloud_absolute_folder.is_dir():
    print(f"nextcloud_absolute_folder is not a directory: {nextcloud_absolute_folder}")
    exit(1)

if args.event_type == "error":
    handled_errors: list = config["handled_errors"]
    error_id, error_message, rel_path = args.event_args
    if error_id not in handled_errors:
        print(f"Ignoring error ID: {error_id}")
        exit(1)
    # Write file with error
    print("Writing error file")
    error_file = nextcloud_absolute_folder / f"{rel_path}.err.txt"
    error_file.write_text(f"{error_id}: {error_message}")
    os.chmod(error_file, 0o666)
# Make nextcloud aware of the file
print("Asking nextcloud to rescan")
subprocess.check_call(
    [
        "docker", "exec", "--user", "www-data", nextcloud_container, "./occ", "files:scan", "-p",
        nextcloud_container_folder
    ]
)
