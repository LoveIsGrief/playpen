"""

"""
from argparse import ArgumentParser

__author__ = "LoveIsGrief"


def main(grundsteuer, grundfrei, spitzensteuer, spitzengrenze, nutzer_einkommen=None):
    # https://de.wikipedia.org/wiki/Grenzsteuersatz
    # f(x) = ax + b
    b = ((grundfrei * spitzensteuer) - (spitzengrenze * grundsteuer)) / (grundfrei - spitzengrenze)
    a = (grundsteuer - b) / grundfrei

    # a = (spitzensteuer - grundsteuer) / (spitzengrenze - grundfrei)

    def hol_reeler_steuersatz(x):
        if x < grundfrei:
            return 0
        elif grundfrei <= x <= spitzengrenze:
            return (a * x) + b
        else:
            return spitzensteuer

    assert (hol_reeler_steuersatz(grundfrei), grundsteuer)
    assert (hol_reeler_steuersatz(spitzengrenze), spitzensteuer)

    def versteuer(einkommen):
        zu_versteuern = max(0.0, einkommen - grundfrei)
        reeler_steuersatz = hol_reeler_steuersatz(einkommen)
        durchschnitt_einkommen = (grundfrei + einkommen) / 2.0
        durchschnitt_steuersatz = hol_reeler_steuersatz(durchschnitt_einkommen)
        steuerbetrag = (zu_versteuern * durchschnitt_steuersatz) / 100.0

        return {
            "zu_versteuern": zu_versteuern,
            "reeler_steuersatz": reeler_steuersatz,
            "durchschnitt_steuersatz": durchschnitt_steuersatz,
            "steuerbetrag": steuerbetrag,
            "rest": einkommen - steuerbetrag,
            "einkommen": einkommen
        }

    print("Einkommen, zu versteuern, reeler Steuersatz, Durchschnittssteuersatz, Steuerbetrag, Rest")
    for simulations_einkommen in range(0, 250000, 5000):
        steuer = versteuer(simulations_einkommen)
        print("%(einkommen)d, %(zu_versteuern).2f, %(reeler_steuersatz).2f, %(durchschnitt_steuersatz).2f, %(steuerbetrag).2f, %(rest).2f" % steuer)


if __name__ == '__main__':
    parser = ArgumentParser(description="Verwenden um einen Steuervorschlag auszutesten")
    parser.add_argument(
        "--grundsteuer",
        type=float, default=40.0,
        help="Grundsteuersatz"
    )
    parser.add_argument(
        "--grundfrei",
        type=float, default=25000.0,
        help="Grundfreibetrag"
    )
    parser.add_argument(
        "--spitzensteuer",
        type=float, default=80.0,
        help="Spitzensteuersatz"
    )
    parser.add_argument(
        "--spitzengrenze",
        type=float, default=65000.0,
        help="Spitzengrenze"
    )

    parser.add_argument(
        "--einkommen",
        help="Wieviel du verdienst"
    )

    args = parser.parse_args()
    print(args)
    main(args.grundsteuer, args.grundfrei, args.spitzensteuer, args.spitzengrenze, args.einkommen)
