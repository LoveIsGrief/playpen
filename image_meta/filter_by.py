#!/usr/bin/env python3

from argparse import ArgumentParser
from pathlib import Path
from sys import stderr


def is_ge(value, min_value, inverse=False):
    if not min_value:
        return True
    res = value >= min_value
    if inverse:
        res = not res
    return res


def main(path, min_width=None, min_height=None, inverse=False):
    # Stored as "lines" in order to be able to handle piped or user input

    # Handle piped input or user input
    if path == "-":
        lines = iter_lines()

    # Handle file or folder
    else:
        path = Path(path)
        if path.is_file():
            lines = path.read_text().splitlines(keepends=False)
        else:
            lines = [item.name for item in path.iterdir()]

    # The actual filtering
    filtered = []
    for line in lines:
        try:
            width, height = line.split('x')
            width = int(width)
            height = int(height)
            width_condition = is_ge(width, min_width, inverse)
            height_condition = is_ge(height, min_height, inverse)

            if width_condition and height_condition:
                filtered.append(line)
                print(line)
        except Exception as e:
            print("Couldn't handle '%s': %s" % (line, e), file=stderr)

    total_filtered = len(filtered)
    print("Total: %s" % total_filtered, file=stderr)

    # Filtering everything out should be an error
    if not total_filtered:
        exit(1)


def iter_lines():
    while True:
        try:
            yield input()
        except EOFError:
            break


if __name__ == '__main__':
    parser = ArgumentParser(description="Filters dimension strings (XxY) by height or width"
                                        " or both and prints them to STDOUT")
    parser.add_argument("-i", "--inverse",
                        action="store_true",
                        help="Filter out instead of filter in")
    parser.add_argument("--min-height", type=int)
    parser.add_argument("--min-width", type=int)
    parser.add_argument("path", help='"-" to read from STDIN, or path to file containing strings'
                                     ', or a directory whose contents will be listed and filtered')
    args = parser.parse_args()

    main(args.path,
         min_width=args.min_width,
         min_height=args.min_height,
         inverse=args.inverse,
         )
