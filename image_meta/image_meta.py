#!/usr/bin/env python3

import math
from argparse import ArgumentParser

from PIL import Image


class Field:
    NAME = None
    ABBREV = None

    def __init__(self, image: Image):
        self.image = image

    def __call__(self, *args, **kwargs):
        print(self.calc())

    def calc(self):
        raise NotImplementedError()


class AspectRatioField(Field):
    NAME = "aspectratio"
    def calc(self):
        width = self.image.width
        height = self.image.height

        gcd = math.gcd(width, height)
        r_width = int(width / gcd)
        r_height = int(height / gcd)
        return f"{r_width}:{r_height}"


class DimensionsField(Field):
    NAME = "dimensions"
    def calc(self):
        return f"{self.image.width}x{self.image.height}"


FIELDS = {
    field.NAME: field for field in [
        AspectRatioField,
        DimensionsField,
    ]
}


def main(image_path, fields):
    image = Image.open(image_path)
    for field in fields:
        field_class = FIELDS[field]
        field_class(image)()


if __name__ == '__main__':
    parser = ArgumentParser(description="Prints meta information for a given image")
    parser.add_argument("-f", "--field", help="Which specific information to print",
                        choices=FIELDS.keys(),
                        action="append")
    parser.add_argument("image")
    args = parser.parse_args()

    main(args.image, args.field or AspectRatioField.NAME)
