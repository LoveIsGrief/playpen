"""
playpen
Copyright (C) 2021 LoveIsGrief

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""
import argparse
import enum
import hashlib
import logging
import sys
from os import environ
from pathlib import Path
from typing import Any, Callable, List, Optional, Sequence

import mutagen.id3
from requests import Response
from requests_toolbelt.sessions import BaseUrlSession


class RequiredEnvVars(enum.Enum):
    AMPACHE_USER = "AMPACHE_USER"
    AMPACHE_API_KEY = "AMPACHE_API_KEY"


HASH_ALGORITHM = "sha256"


def main():
    check_env_vars()

    ###############
    # Get params
    ###############

    parser = argparse.ArgumentParser()
    parser.add_argument("--host", default="localhost")
    parser.add_argument("filename")
    parser.add_argument("rating", type=int)

    args = parser.parse_args()
    ampache_host = environ.get("AMPACHE_HOST", args.host)
    file_path = Path(args.filename).resolve(strict=True)
    rating = args.rating

    contents = file_path.read_text()

    session = get_base_session(ampache_host)
    handle_file(contents.split("\n"), rating, session)


def check_env_vars():
    global value
    for env_var in RequiredEnvVars:
        value = env_var.value
        if value not in environ:
            logging.info("Require env var: %s" % value, file=sys.stderr)
            exit(1)


def get_base_session(ampache_host: str) -> BaseUrlSession:
    ###############
    # Handshake
    ###############

    logging.info("handshake with %s" % ampache_host)

    api_key = environ[RequiredEnvVars.AMPACHE_API_KEY.value]
    key_hasher = hashlib.new(HASH_ALGORITHM, api_key.encode())
    key_hash = key_hasher.hexdigest()

    pass_to_hash = "%s%s" % (environ[RequiredEnvVars.AMPACHE_USER.value], key_hash)
    passphrase_hasher = hashlib.new(HASH_ALGORITHM, pass_to_hash.encode())
    passphrase_hash = passphrase_hasher.hexdigest()

    # A reusable session that prefixes the ampache service URL
    base_session = BaseUrlSession(f"http://{ampache_host}/server/json.server.php")
    # Get auth key that'll have to be used in all requests
    auth_result = base_session.get("", params=dict(action="handshake", auth=passphrase_hash,
                                                   version=350001))
    auth_json = get_ampache_json(auth_result)
    base_session.params["auth"] = auth_json["auth"]
    return base_session


def get_ampache_json(res: Response):
    res.raise_for_status()
    res_json = res.json()
    if "error" in res_json:
        raise Exception("Bad request", res_json)
    return res_json


# https://picard-docs.musicbrainz.org/v2.4/en/appendices/tag_mapping.html#id21
TRACK_ID_TAGS = [
    "musicbrainz_trackid",
    "MusicBrainz/Release Track Id",
    "----:com.apple.iTunes:MusicBrainz Track Id",
    "TXXX:MusicBrainz Release Track Id",
]


def handle_file(lines: Sequence[str], rating: int, session: BaseUrlSession):
    for line in lines:
        line = line.strip()
        # ignore comments
        if not line or line.startswith("#"):
            continue

        try:
            tags = mutagen.File(line, easy=True)
        except mutagen.MutagenError:
            logging.exception("Couldn't handle file %s", line)
            continue

        if tags is None:
            logging.warning("Couldn't read tags for %s", line)
            continue

        title = get_tag(tags, "TIT2", "title", "Title")
        artist = get_tag(tags, "TPE1", "artist", "Author")
        track_id = get_tag(tags, *TRACK_ID_TAGS)

        if track_id:
            logging.info("Rating '%s' by '%s': %s", title, artist, track_id)
            try:
                songs = find_ampache_song(track_id, session)
                for song in songs:
                    rate_ampache_song(song.get("id"), rating, session)
            except:
                logging.exception("Couldn't rate %s", line)
        else:
            logging.warning("NO TRACK ID '%s' by '%s'", title, artist)
            logging.warning(line)


def tag_to_str(tag: Any) -> Optional[str]:
    if isinstance(tag, list):
        return next(iter(tag), None)
    elif isinstance(tag, str):
        return tag
    else:
        return None


def get_tag(mutagen_file: mutagen.FileType, *options: List[str],
            transform_func: Callable = tag_to_str):
    tag = None
    for option in options:
        tag = mutagen_file.get(option)
        if tag:
            break

    return transform_func(tag) if transform_func else tag


def rate_ampache_song(ampache_id: str, rating: int, session: BaseUrlSession):
    """

    https://ampache.org/api/api-json-methods#rate
    """
    # TODO: Find the song ID on ampache
    # TODO: Rate the found song
    response = session.get("", params=dict(
        action="rate", type="song", id=ampache_id, rating=rating
    ))
    res_json = get_ampache_json(response)

    return res_json


def find_ampache_song(musicbrainz_id: str, session: BaseUrlSession):
    """
    Use advanced search to find tracks with the given musicbrainz track ID

    https://ampache.org/api/api-json-methods#advanced_search
    https://ampache.org/api/api-advanced-search
    """
    response = session.get("", params=dict(
        action="advanced_search",
        operator="and",
        rule_1="mbid",
        rule_1_operator="4",  # is
        rule_1_input=musicbrainz_id,
    ))
    res_json = get_ampache_json(response)
    if "song" not in res_json:
        raise Exception("Expected songs in result", res_json)
    return res_json["song"]


if __name__ == '__main__':
    logging.basicConfig(level=logging.INFO)
    main()
