import json
import logging
import os
import subprocess
import sys
from time import sleep

from praw import Reddit
from youtube_dl import YoutubeDL

JSON_PATH = "subbredit_submission.json"


def get_best_video(url):
    expected_format = "bestvideo/best"
    with YoutubeDL({"format": expected_format}) as ydl:
        format_selector = ydl.build_format_selector(expected_format)
        res = ydl.extract_info(url, download=False)
        formats = res["formats"]
        incomplete_formats = (
            # All formats are video-only or
                all(f.get('vcodec') != 'none' and f.get('acodec') == 'none' for f in formats)
                # all formats are audio-only
                or all(f.get('vcodec') == 'none' and f.get('acodec') != 'none' for f in formats))

        ctx = {
            'formats': formats,
            'incomplete_formats': incomplete_formats,
        }
        return next(format_selector(ctx))


def build_json(title, source_url, reddit_path, reddit_user, subreddit, json_path):
    description = """Mirrored from %(source_url)s

Originally posted to %(reddit_url)s by %(reddit_user)s
as %(title)s
""" % {
        "source_url": source_url,
        "reddit_url": "https://reddit.com/%s" % reddit_path,
        "reddit_user": "https://reddit.com/u/%s" % reddit_user,
        "title": title
    }

    json_str = json.dumps({
        # Max 120 chars in title
        "name": title[:120],
        "category": "10",
        "language": "en",
        "tags": [
            "reddit",
            "/r/%s" % subreddit,
            subreddit,
        ],
        "description": description,
        "waitTranscoding": True
    }, indent=2)
    with open(json_path, "w") as file_json:
        file_json.write(json_str)
    logging.info("Wrote json for %s", title)
    logging.debug(json_str)


def import_url(url, title):
    proc = subprocess.run([
        "ptu",
        "import",
        "--json", JSON_PATH,
        "--skip", "same_name_on_server",
        url
    ], stderr=subprocess.PIPE, stdout=subprocess.PIPE, env=os.environ)

    proc_std = proc.stdout.decode()
    if proc_std:
        print("stdout: %s" % proc_std)
    proc_err = proc.stderr.decode()
    if proc_err:
        print("stderr: %s" % proc_err, file=sys.stderr)
    if proc.returncode == 0:
        logging.info("uploaded %s", title)
    else:
        raise proc.check_returncode()


def main(subreddit_name):
    reddit = Reddit(user_agent='PeerTube Subreddit Mirror',
                    client_id=os.getenv('REDDIT_CLIENT_ID'), client_secret=os.getenv('REDDIT_CLIENT_SECRET'),
                    )
    subreddit = reddit.subreddit(subreddit_name)
    for submission in subreddit.top("day"):
        logging.info("Handling submission '%s': %s", submission.title, submission.url)
        if submission.is_self:
            continue
        try:
            title = "/r/%s: %s" % (subreddit_name, submission.title)
            build_json(
                # Max 120 chars for title
                title,
                submission.url,
                submission.permalink,
                submission.author.name,
                subreddit_name,
                JSON_PATH,
            )
            try:
                best_video = get_best_video(submission.url)
            except Exception as e:
                logging.warning("Error getting best video: %s", e)
                raise
            # print("Best video for %s: %s" % (submission.title, best_video))
            try:
                import_url(best_video["url"], title)
            except Exception as e:
                logging.warning("Error importing: %s", e)
                raise
            logging.info("Sleeping 20 seconds")
            print('\n')
            sleep(20)
        except:
            logging.warning("Couldn't process video for: %s", submission.url)
            logging.info("Sleeping 10 seconds")
            sleep(10)


if __name__ == '__main__':
    logging.basicConfig(level=logging.INFO)
    main(sys.argv[1])
