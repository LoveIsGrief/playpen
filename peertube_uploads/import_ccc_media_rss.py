"""
Copyright (c) 2019 LoveIsGrief

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
"""

import argparse
import html
import json
import subprocess
import sys
import xml.etree.ElementTree as ET
from pathlib import Path
from tempfile import mkdtemp
from time import sleep
from xml.etree.ElementTree import Element

import requests
from bs4 import BeautifulSoup
from jinja2 import Template

DESC_TEMPLATE = Template("""{{subtitle}}

{{description}}

-----------
Source: {{url}}
""")


def make_finder(item, prefix):
    def finder(name):
        return item.find("{%(prefix)s}%(name)s" % {
            "prefix": prefix,
            "name": name
        })

    return finder


def check_path(path):
    p = Path(path)
    if not p.is_dir():
        raise argparse.ArgumentTypeError("Path isn't a directory")
    return p


def main(url, cache_dir=None):
    if not cache_dir:
        cache_dir = Path(mkdtemp(suffix="-upload_ccc"))
    print("Using cache directory: %s" % cache_dir)

    r = requests.get(url)
    root = ET.fromstring(r.text)  # type: Element

    for item in root.iterfind("./channel/item"):
        itunes = make_finder(item, "http://www.itunes.com/dtds/podcast-1.0.dtd")
        dc = make_finder(item, "http://purl.org/dc/elements/1.1/")

        name = item.find("title").text
        id_ = dc("identifier").text
        json_path = cache_dir.joinpath("%s.json" % id_)
        done_path = json_path.with_name("%s.done" % id_)
        err_path = json_path.with_name("%s.err" % id_)

        print("handling video: %s" % name)

        if done_path.is_file():
            print("\talready done")
            continue

        if err_path.is_file():
            print("\talready had error while processing")
            continue

        subtitle_el = itunes("subtitle")
        subtitle = ""
        if subtitle_el:
            subtitle = html.unescape(subtitle_el.text)
        bs = BeautifulSoup(item.find("description").text, features="lxml")
        description = bs.get_text()
        j = {
            "name": name,
            "description": DESC_TEMPLATE.render({
                "subtitle": subtitle,
                "description": html.unescape(description),
                "url": item.find("link").text
            }),
            "tags": [
                        tag.strip()
                        for tag in itunes("keywords").text.split(",")
                    ] + ["chaos computer club"],
            "license": "4",  # CC NC - for some reason this doesn't work 😢
            "category": "15",  # Science and tech
            "waitTranscoding": True
        }

        json_path.write_text(json.dumps(j))

        url = item.find("guid").text
        completed = subprocess.run([
            "ptu", "import",
            "--skip", "same_name_on_server",
            "--json", str(json_path),
            url
        ], stdout=subprocess.PIPE, stderr=subprocess.STDOUT)

        if completed.returncode == 0:
            done_path.write_bytes(completed.stdout)
            print("\tSUCCESS-->waiting 30 seconds")
            sleep(30)
        else:
            err_path.write_bytes(completed.stdout)
            print("\tERROR-->waiting 10 seconds", file=sys.stderr)
            sleep(10)


if __name__ == '__main__':
    parser = argparse.ArgumentParser(
        description="Upload videos from media.ccc.de media RSS xmls"
    )
    parser.add_argument("-c", "--cachedir", type=check_path)
    parser.add_argument("url")

    args = parser.parse_args()
    main(args.url, args.cachedir)
