"""
Copyright (c) 2019 LoveIsGrief

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
"""

import argparse
import json
import subprocess
import urllib.parse as urlparse
from pathlib import Path
from subprocess import PIPE
from sys import stderr
import os

from jinja2 import Template

DESC_TEMPLATE = Template("""source: https://media.defcon.org/DEF%20CON%2026/DEF%20CON%2026%20villages/{{ filename }}

Torrent with all media: https://media.defcon.org/DEF%20CON%20Torrents/DEF%20CON%2026.torrent
""")


def build_json(vid_path):
    description = DESC_TEMPLATE.render({
        "filename": urlparse.quote(vid_path.name)
    })

    json_str = json.dumps({
        "name": vid_path.stem,
        "category": "15",
        "language": "en",
        "tags": [
            "DEFCON",
            "conference",
            "hacking",
            "talk"
        ],
        "description": description,
        "waitTranscoding": True
    }, indent=2)
    with open(str(vid_path) + ".json", "w") as file_json:
        file_json.write(json_str)
    print("Wrote json for %s" % vid_path)
    print(json_str)


def upload_vid(vid_path):
    proc = subprocess.run([
        "peertube-uploader.py",
        "-f", str(vid_path),
        "-j", str(vid_path.with_name(vid_path.name + ".json")),
        "--skip", "same_name_on_server"
    ], stderr=PIPE, stdout=PIPE, env=os.environ)
    vid_path.with_name(vid_path.name + ".err").write_bytes(proc.stderr)
    if proc.returncode == 0:
        print("uploaded %s" % vid_path)
        vid_path.with_name(vid_path.name + ".done").write_bytes(proc.stdout)
    else:
        raise proc.check_returncode()


def main(dir_path):
    root = Path(dir_path)
    if not root.is_dir():
        print("directory really has to be a dir >_>")
    for _file in root.iterdir():
        if not _file.is_file():
            continue
        if not _file.suffix.endswith("mp4"):
            print("ignoring %s" % _file)
            continue

        try:
            if not _file.with_name(_file.name + ".json").exists():
                build_json(_file)
            else:
                print("already jsoned %s" % _file)
        except Exception as e:
            print("Couldn't build_json file %s: %s" % (_file, e), file=stderr)
            continue

        try:
            if not _file.with_name(_file.name + ".done").exists():
                upload_vid(_file)
            else:
                print("already uploaded %s" % _file)
        except Exception as e:
            print("Couldn't upload file %s: %s" % (_file, e), file=stderr)


if __name__ == '__main__':
    parser = argparse.ArgumentParser(
        description="Upload DEFCON villages"
    )
    parser.add_argument("dir")

    args = parser.parse_args()
    main(args.dir)
