"""
Copyright (c) 2019 LoveIsGrief

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
"""

import argparse
import json
import re
import subprocess
from pathlib import Path
from sys import stderr

from jinja2 import Template
from requests_html import HTMLSession

FILE_REGEX = re.compile(r"25c3-(?P<id>\d+)-(?P<lang>\w+)-(?P<title>[\w_-]+)\.mp4$")
TITLE_REPL_REGEX = re.compile(r"[_-]")
REPLACEMENT_URL = "https://fahrplan.events.ccc.de/congress/2008/Fahrplan/events/{id}.{lang}.html"
DESC_TEMPLATE = Template("""{{description}}

{% if attachments -%}
Attachments:
{%- for attachment in attachments %}
    - {{ attachment.text }} : {{ attachment.attrs.href }}
{%- endfor %}
{%- endif %}

{% if links -%}
Links:
{%- for link in links %}
    - {{ link.text }} : {{ link.attrs.href }}
{%- endfor %}
{%- endif %}

-----------
Source: {{url}}
""")


def build_json(vid_path, session, match):
    talk_id = match.group("id")
    talk_lang = match.group("lang")

    url = REPLACEMENT_URL.format(id=talk_id, lang=talk_lang)
    r = session.get(url)
    html = r.html

    talk_title = html.find(".title.summary", first=True).text
    html_description = html.find(".description", first=True).text
    attachment_anchors = html.find(".attachments a")
    link_anchors = html.find(".links a")
    description = DESC_TEMPLATE.render({
        "url": url,
        "description": html_description,
        "attachments": attachment_anchors,
        "links": link_anchors
    })

    json_str = json.dumps({
        "name": talk_title,
        "category": 15,
        "license": 6,
        "language": "German" if talk_lang == "de" else "English",
        "tags": [
            "conference",
            "Chaos Computer Club",
            "talk"
        ],
        "description": description,
        "waitTranscoding": True
    }, indent=2)
    with open(str(vid_path) + ".json", "w") as file_json:
        file_json.write(json_str)
    print("Wrote json for %s" % vid_path)
    print(json_str)


def upload_vid(vid_path):
    subprocess.run([
        "./peertube-uploader.py",
        "-f", str(vid_path),
        "-j", str(vid_path.with_name(vid_path.name + ".json"))
    ], check=True)
    print("uploaded %s")
    vid_path.with_name(vid_path.name + ".done").write_text('')


def main(dir_path):
    root = Path(dir_path)
    if not root.is_dir():
        print("directory really has to be a dir >_>")
    s = HTMLSession()
    s.headers.update({
        "User-Agent": "curl/7.58.0",
        "Accept": "*/*"
    })

    for _file in root.iterdir():
        if not _file.is_file():
            continue
        m = FILE_REGEX.match(_file.name)
        if not m:
            print("ignoring %s" % _file)
            continue

        try:
            if not _file.with_name(_file.name + ".json").exists():
                build_json(_file, s, m)
            else:
                print("already jsoned %s" % _file)
            if not _file.with_name(_file.name + ".done").exists():
                upload_vid(_file)
            else:
                print("already uploaded %s" % _file)
        except Exception as e:
            print("Exception: %s" % e, file=stderr)
            print("Couldn't build_json file %s" % _file, file=stderr)


if __name__ == '__main__':
    parser = argparse.ArgumentParser(
        description="Get information about the 25C3 presentations"
    )
    parser.add_argument("dir")

    args = parser.parse_args()
    main(args.dir)
