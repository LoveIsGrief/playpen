"""
playpen
Copyright (C) 2020 LoveIsGrief

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""
import argparse
import logging
import subprocess
from configparser import ConfigParser
from pathlib import Path

from gitlab import Gitlab

DOT_GIT = ".git"


class RepoArgs:
    def __init__(self, url, path, branch):
        self.url = url
        self.path = Path(path)
        self.branch = branch


def _run(*args):
    return subprocess.run([str(arg) for arg in args]).check_returncode()


def clone(repo):
    repo.path.parent.mkdir(exist_ok=True, parents=True)
    return _run(
        "git",
        "clone",
        repo.url,
        repo.path,
    )


def git_clean(repo_path):
    return _run(
        "git",
        "-C", repo_path,
        "clean",
        "-dfq"
    )


def merge(repo_path, url, branch):
    return _run(
        "git",
        "-C", repo_path,
        "pull",
        url,
        branch
    )


def pull(repo):
    return _run(
        "git",
        "-C", repo.path,
        "pull"
    )


def checkout(fork_repo):
    return _run(
        "git",
        "-C", fork_repo.path,
        "checkout",
        fork_repo.branch
    )


def push(path):
    return _run(
        "git",
        "-c", "receive.denyCurrentBranch=ignore",
        "-C", path,
        "push"
    )


def create_mr(source_repo, fork_repo):
    g = Gitlab.from_config("gitlab")
    project_name = fork_repo.url.split('/')[-1]
    if project_name.endswith(DOT_GIT):
        project_name = project_name[:-len(DOT_GIT)]
    g.auth()
    user = g.users.get(g.user.id)
    projects = user.projects.list(search=project_name)
    project = projects[0]  # gitlab.v4.objects.Project
    project = g.projects.get(project.id)
    project.mergerequests.create(data=dict(
        title=f"Merge {source_repo.branch} into {fork_repo.branch}",
        description="Automatically created due to merge conflict",
        id=project.get_id(),
        source_branch=source_repo.branch,
        target_branch=fork_repo.branch
    ))


def merge_branches(source_repo, fork_repo):
    """

    :param source_repo:
    :type source_repo: RepoArgs
    :param fork_repo:
    :type fork_repo: RepoArgs
    :return:
    :rtype:
    """
    if not fork_repo.path.exists():
        # clone fork
        clone(fork_repo)
    else:
        pull(fork_repo)

    # Clean current branch
    git_clean(fork_repo.path)

    # Checkout fork branch
    checkout(fork_repo)

    # try to merge from source repo
    try:
        merge(fork_repo.path, source_repo.url, source_repo.branch)
        # Push changed forked_repo branch
        try:
            push(fork_repo.path)
        except Exception as e:
            logging.error("Couldn't push to fork: %s", e)
    except:
        # Create MR on gitlab
        create_mr(source_repo, fork_repo)


def main(conf_file):
    """
    :type conf_file: ConfigParser
    """
    logging.basicConfig(level=logging.INFO)
    source_repo = RepoArgs(**conf_file["source"])
    fork_repo = RepoArgs(**conf_file["fork"])

    merge_branches(source_repo, fork_repo)


def read_conf_file(filename):
    with open(filename) as f:
        config = ConfigParser()
        config.read_file(f)
        return config


if __name__ == '__main__':
    parser = argparse.ArgumentParser(
        prog="merge_branches",
        description="""
A tool to help keeping forks that have changes with their upstream repos.
If there are no merge conflicts, the upstream changes
 are integrated into the fork and pushed.
Merge conflicts require human resolution and thus
 a merge request is created on gitlab.
 

It's written to be called in a cron-job
    """)
    parser.add_argument("config", help="Configuration file", type=read_conf_file)
    parsed_args = parser.parse_args()
    main(parsed_args.config)
