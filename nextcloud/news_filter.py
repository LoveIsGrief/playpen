#!/usr/bin/env python3
"""
news_filter.py
Copyright (C) 2021 LoveIsGrief

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""
import argparse
import base64

from requests_toolbelt import sessions

parser = argparse.ArgumentParser(
    description="Simple script to filter Nextcloud news items by marking them as read")

parser.add_argument("-c", "--contains", required=True,
                    help="Filters out items with titles that contain this. "
                         "Can be used multiple times (OR): only one has to match",
                    action="append")

parser.add_argument("host", help="e.g https://your-nextcloud.com")
parser.add_argument("username", help="The user to login with")
parser.add_argument("password", help="The password to login with")

args = parser.parse_args()

# A session that allows appending paths to a base path
s = sessions.BaseUrlSession(base_url=f"{args.host}/index.php/apps/news/api/v1-2/")
credentials = base64.b64encode(f"{args.username}:{args.password}".encode()).decode()
s.headers["Authorization"] = f"Basic {credentials}"

# ALL unread items
res = s.get("items", allow_redirects=False, params={
    "batchSize": -1,
    "type": 3,  # the type of the query (Feed: 0, Folder: 1, Starred: 2, All: 3)
    "id": 0,  # the id of the folder or feed, Use 0 for Starred and All
    "getRead": False,
})
res.raise_for_status()
result = res.json()

# Workaround to /items?getRead=False also returning read items
# https://github.com/nextcloud/news/issues/1328
# Don't mess with starred articles either
unread_unstarred_items = [
    item for item in result.get("items", [])
    if item.get("unread") is True and item.get("starred") is False
]
print("Unread items: ", len(unread_unstarred_items))

to_mark = []
for item in unread_unstarred_items:
    title = item.get("title", "").lower()
    if any(query in title for query in args.contains):
        id_ = item["id"]
        to_mark.append(id_)
        mark_res = s.put(f"items/{item['id']}/read")
        print(id_, mark_res.status_code, mark_res.text)
        # time.sleep(0.2)  # If there's rate limiting, this might help

print("Filtered ", len(to_mark))
