#!/usr/bin/env python3

import argparse
import os
import sys
from pathlib import Path

parser = argparse.ArgumentParser(
    description="Links unlinked files in one folder with files of the same name in another"
)
parser.add_argument("paths", nargs=2, help="left dir will get hard links from right dir")

args = parser.parse_args()
left_dir, right_dir = args.paths
left_dir = Path(left_dir)
right_dir = Path(right_dir)

if not (left_dir.is_dir() and right_dir.is_dir()):
    print("Both have to be dirs, you bozo", file=sys.stderr)
    exit(1)

for left_item in left_dir.iterdir():
    if left_item.is_dir() or left_item.stat().st_nlink != 1:
        continue

    print("Replacing %s" % left_item)
    right_item = right_dir / left_item.name

    if not right_item.is_file():
        print("\tRight isn't a file: %s" % right_item)
        continue
    left_item.unlink()
    os.link(right_item, left_item)
