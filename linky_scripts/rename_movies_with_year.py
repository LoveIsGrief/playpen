"""
Copyright (c) 2019 LoveIsGrief

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
"""

import argparse
import re
import subprocess
from pathlib import Path

import tmdbsimple as tmdb

YEAR_REGEX = re.compile(r"\((\d+)\)$")


def dir_path_type(arg):
    path = Path(arg).resolve(True)
    if not path.is_dir():
        raise argparse.ArgumentTypeError("Path must be a directory")
    return path


def get_year(name, search):
    """
    Searches tmdb and presents the results in a list that the user can choose from.

    :return: The year of the selected movie
    :rtype: basestring | None
    """
    results = search.movie(query=name)["results"]
    if len(results) == 0:
        print("No results")
        return None

    enum_results = {
        str(i): result
        for i, result in enumerate(results, 1)
    }
    choice = None
    while choice not in enum_results:
        for i, result in enum_results.items():
            print("\t%s: %s (%s) %s " % (
                i,
                result["title"],
                result.get("release_date"),
                "https://www.themoviedb.org/movie/" + str(result["id"])
            ))
        choice = input("Choose the best match for '%s': " % name).strip()
        if choice == "":
            return None

    release_date = enum_results[choice].get("release_date")
    if not release_date:
        print("Maybe next time pick something with a release date?")
        return None
    print("Release date: %s" % release_date)
    print("Year: %s" % release_date)
    return release_date.split("-")[0]


def _exec(cmd_args):
    """
    Executes a command and prints the output and return code
    :param cmd_args: Will be passed to subprocess
    :type cmd_args: list[basestring]
    """
    print("Executing: %s" % cmd_args)
    res = subprocess.run(cmd_args)
    print("Returncode: %s" % res.returncode)


def main(dir_path, api_key):
    tmdb.API_KEY = api_key

    search = tmdb.search.Search()
    for item in dir_path.iterdir():
        # Must end with "(<year>)"
        old_name = item.name
        if not (item.is_dir() and not YEAR_REGEX.match(old_name)):
            continue
        year = get_year(old_name, search)
        if year:
            print("Year: %s" % year)
        else:
            print("Skipping '%s'..." % old_name)
            continue

        # Rename
        new_item = item.parent / ("%s (%s)" % (old_name, year))
        _exec([
            "linky", "move",
            str(item), str(new_item)
        ])

        # Tag with the selected year
        _exec([
            "linky", "tag",
            "-t", "Year/%s" % year,
            str(new_item)
        ])

    print("Done!")


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description="""
    Tack on the year in brackets to movie folders managed by linky.
    The Movie DB is used as a source for the year.
    """)
    parser.add_argument("-t", "--tmdb", help="TMDB API key", required=True)
    parser.add_argument(
        "directory",
        help="Contains the movie folders",
        type=dir_path_type
    )

    args = parser.parse_args()

    main(args.directory, args.tmdb)
