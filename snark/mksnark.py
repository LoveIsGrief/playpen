"""
Copyright (c) 2019 LoveIsGrief

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
"""

import argparse
import os
from pathlib import Path
from sys import stderr

from requests_html import HTMLSession


def handle_dir(rel_path, base, url):
    """
    Step through a directory and create torrents out of each file

    :type rel_path: Path
    :type base: Path
    :param url: I2P url
    :type url: basestring
    """
    full_path = base / rel_path
    for item in full_path.iterdir():
        rel_item = item.relative_to(base)
        if item.is_file():
            handle_file(rel_item, url)
        elif item.is_dir():
            handle_dir(rel_item, base, url)


def handle_file(file_path, url):
    """
    Create a torrent using I2P from the file

    :type file_path: Path
    :type url: I2P url
    """
    if file_path.is_absolute():
        raise ValueError("Cannot pass relative paths to i2psnark")

    print("Making torrent from: %s" % file_path)
    file_name = file_path.name

    # Get ourselves the nonce from the form
    hs = HTMLSession()
    html = hs.get(url).html
    nonce_el = html.find(".snarkNewTorrent form input[name=nonce]", first=True)
    nonce = nonce_el.attrs.get("value")

    ret = hs.post("%s/_post" % url, data={
        "nonce": nonce,
        "action": "Create",
        "nofilter_baseFile": str(file_path),
        "foo": "Create+torrent",
        "announceURL": "http://tracker2.postman.i2p/announce.php",
        "backup_http://ri5a27ioqd4vkik72fawbcryglkmwyy4726uu5j3eg6zqh2jswfq.b32.i2p/announce": "foo",
        "backup_http://w7tpbzncbcocrqtwwm3nezhnnsw4ozadvi2hmvzdhrqzfxfum7wa.b32.i2p/a": "foo",
        "backup_http://diftracker.i2p/announce.php": "foo",
        "backup_http://uajd4nctepxpac4c4bdyrdw7qvja2a5u3x25otfhkptcjgd53ioq.b32.i2p/announce": "foo",
        "backup_http://s5ikrdyjwbcgxmqetxb3nyheizftms7euacuub2hic7defkh3xhq.b32.i2p/a": "foo",
    })

    ret.raise_for_status()

    # Try and output if we were successful by reading the logs
    messages = [message.text for message in ret.html.find(".snarkMessages ul li")]

    i = None
    for _i, message in enumerate(messages):
        if file_name in message:
            i = _i
            break
    if i is not None:
        for message in reversed(messages[:i + 1]):
            print("\t%s" % message)
    else:
        print("\tNo mention of %s" % file_name, file=stderr)


def check_path_arg(path):
    path = Path(path)
    if not (path.is_file() or path.is_dir()):
        raise argparse.ArgumentTypeError("Path must be a file or directory")
    return path


def check_base(base):
    base = Path(base)
    if not base.is_dir():
        raise argparse.ArgumentTypeError("Path must be a directory")
    return base


def main(args):
    rel_path = args.path  # type: Path
    base = args.base

    # Convert to relative path
    if rel_path.is_absolute():
        full_path = rel_path
        check_path_arg(rel_path)
        rel_path = rel_path.relative_to(base)
    else:
        full_path = base / rel_path
        check_path_arg(full_path)

    if full_path.is_dir():
        handle_dir(rel_path, base, args.url)
    else:
        handle_file(rel_path, args.url)


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description="Contacts i2psnark to create a torrent using HTTP POST")

    parser.add_argument("-b", "--base",
                        help="Where the base directory for saved torrents is."
                             "Only required if the given path is absolute",
                        type=check_base,
                        default=os.getenv("SNARK_TORRENT_BASE", os.getcwd()))
    parser.add_argument("-u", "--url",
                        help="The I2P snark URL",
                        default=os.getenv("SNARK_URL", "http://localhost:7657/i2psnark"))
    parser.add_argument("path", help="The directory containing files, or the file to upload. "
                                     "This will be transformed to a relative path for i2psnark "
                                     "that's relative to where its torrents are stored.",
                        type=Path)

    main(parser.parse_args())
